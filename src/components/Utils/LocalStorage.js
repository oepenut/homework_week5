const useLocalStorage = (key, object) => {
  localStorage.setItem(key, object);
}

export default useLocalStorage
