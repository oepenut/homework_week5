import React, { useState } from "react";
import useLocalStorage from "../Utils/LocalStorage.js"
import "./styleCards.scss";

export default function Card(props) {
  const { id, title, imgUrl, gameUrl, body, platform, date, category, comments, setCurrentRoute, apiState } = props
  const [clicked, setClicked] = useState(false);

  const handleClick = () => {
    setClicked(!clicked);
  };


  const LocalStorageData = () => {
    let infoGame = {
      id: id,
      title: title,
      imgUrl: imgUrl,
      gameUrl: gameUrl,
      body: body,
      platform: platform,
      date: date,
      category: category,
      comments: comments,
    };
    useLocalStorage("game", JSON.stringify(infoGame));
    useLocalStorage("stateDetails", true)
  };

  const functionClick = (e) => {
    e.preventDefault();
    LocalStorageData();
    setCurrentRoute("Details");
  };

  return (
    <div className="product">
      <a href={gameUrl}>
        <div className="product_img">
          <img src={apiState === "Arrived" ? imgUrl : "https://i.gifer.com/3sqI.gif"} alt=""></img>
        </div>
      </a>
      <div className="product_footer">
        <h1> {title} </h1>
        <p className="body"> {body} </p>
        <p className="platform">{date}</p>
        <p className="platform">{category}</p>
      </div>
      <div className="button">
        <button
          className="btn"
          clicked={clicked.toString()}
          onClick={handleClick}
        >
          {platform}
        </button>

        <div>
          <a href="" className="btn" onClick={functionClick}>
            View
          </a>
        </div>
      </div>
    </div>
  );
}
