import React, { useState, useEffect } from "react";
import "./stylePaginator.scss";

const DataLengt = () => {
  const [dataSize, setDataSize] = useState([]);

  useEffect(() => {
    const ArraySize = async () => {
      const dataBase = await ObtainDataSize();
      setDataSize(dataBase);
    };
    ArraySize();
  }, []);

  const ObtainDataSize = async () => {
    const response = await fetch(
      "https://trainee-gamerbox.herokuapp.com/games?_sort=id"
    );
    const data = await response.json();
    let number = data.length;
    return number;
  };

  return dataSize;
};

const Pagination = (props) => {
  const { currentPage, setCurrentPage, pageSelection, } = props;


  let sizeData = DataLengt();
  let maxNumberPage = Math.ceil(sizeData / 6);
  let arrayPages = [];

  for (let i = 0; i < maxNumberPage; i++) {
    let a = {
      id: i + 1,
      numberPage: i + 1,
    };

    arrayPages.push(a);
  }


  const [pagePosition, setPagePosition] = useState(currentPage);
  const [clicked, setClicked] = useState("false");

  const HandleChange = (page) => {
    setCurrentPage(page)
    setPagePosition(page)

  };

  const nextPage = () => {
    const nextPage = currentPage + 1;

    if (currentPage < maxNumberPage) {
      setCurrentPage(nextPage);
      setPagePosition(nextPage);
      setClicked(true);

    }
  };

  const prevPage = () => {
    let prevPage = currentPage - 1;
    if (currentPage > 1) {
      setCurrentPage(prevPage);
      setPagePosition(prevPage);
      setClicked(true);
    }
  };

  return (
    <div className={pageSelection}>
      <div className="pagination">
        <button className="BtnNextPrev" onClick={prevPage}>
          PREVIOUS
        </button>

        {arrayPages.length
          ? arrayPages?.map((page) => {
            const { id, numberPage } = page
            return (
              <button
                key={id}
                clicked={clicked.toString()}
                onClick={(() => HandleChange(numberPage))}
                className={`pageNumberList ${pagePosition === id && clicked ? " active" : ""
                  }`}
              >
                {numberPage}
              </button>
            );
          })
          : null}

        <button className="BtnNextPrev" onClick={nextPage}>
          NEXT
        </button>
      </div>
    </div>
  );
};

export default Pagination;