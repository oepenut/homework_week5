import React from "react";
import "./LoaderStyle.scss";

const Loader = (props) => {
  const { apiState } = props;
  return (
    <div className={apiState === "loading" ? "loading" : "Arrived"}>
      LOADING...

      <span className="loaderStyle"> </span>

    </div>

  );
};

export default Loader;
