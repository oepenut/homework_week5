import React from "react";
import Pagination from "../Pagination";
import "./styleFooter.scss";


const Footer = (props) => {
  const { currentPage, nextPage, prevPage, setCurrentPage, pageSelection, } = props;

  return (
    <div className={`footer-${pageSelection}`}>
      <Pagination
        currentPage={currentPage}
        nextPage={nextPage}
        prevPage={prevPage}
        setCurrentPage={setCurrentPage}
        pageSelection={pageSelection}

      ></Pagination>
    </div>
  );
};

export default Footer;
