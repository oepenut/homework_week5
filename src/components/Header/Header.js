import React, { useState } from "react";
import "./styleHeader.scss";
import BurguerButton from "./BurguerButton";

const Header = (props) => {
  const { setCurrentRoute, loginStatus, userData } = props;
  const [clicked, setClicked] = useState(false);
  const handleClick = () => {
    //Cuando esta true lo pasa a false y viceversa
    setClicked(!clicked);
    setCurrentRoute("Home");
  };

  const handleChange = (page) => {
    setCurrentRoute(page);
  }
  return (
    <header>
      <div className="navContainer">
        <h2>
          VideoGames <span> Stores </span>
        </h2>
        <div className={`${loginStatus === "SessionON" ? "loggedUser" : "invisible"}`}> <p> User: {userData.user?.firstName + " " + userData.user?.lastName} </p></div>
        <div className={`links ${clicked ? "active" : ""}`}>

          <a onClick={() => handleChange("Home")}>
            Home
          </a>
          <a href="https://www.freetogame.com/">
            Oficial Site
          </a>
          <a onClick={() => handleChange("Login")}> Login </a>
          <a onClick={() => handleChange("SingUp")} > Sing Up </a>

        </div>

        <div className="burguer">
          {/* Estas son las props */}
          <BurguerButton
            clicked={clicked}
            handleClick={handleClick}
          ></BurguerButton>
        </div>

        <div className={`BgDiv initial ${clicked ? " active" : ""}`}></div>
      </div>
    </header>
  );
};

export default Header;
