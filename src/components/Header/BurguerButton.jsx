import React from "react";
import "../../index.scss";

function BurguerButton({ handleClick, clicked }) {
  return (
    <div
      onClick={handleClick}
      className={`icon nav-icon-5 ${clicked ? "open" : ""}`}
    >
      <span></span>
      <span></span>
      <span></span>
    </div>
  );
}
export default BurguerButton;
