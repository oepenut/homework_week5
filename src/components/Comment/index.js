import React, { useState, useEffect } from "react";
import Loader from '../Loader/Loader.js'
import './styleComments.scss'

const Comment = (props) => {
  const { id } = props

  const [comments, setComments] = useState([]);
  const [apiCommentsState, setApiCommentsState] = useState("loading")

  useEffect(() => {

    (async () => {
      const dataBase = await ObtainCommentsGame();
      setComments(dataBase);
      setApiCommentsState("Arrived")

    })();

  }, [id]);

  const ObtainCommentsGame = async () => {
    setApiCommentsState("loading");
    let URL = `https://trainee-gamerbox.herokuapp.com/games/${id}/comments?_sort=updated_at`
    const response = await fetch(`${URL}`);
    const data = await response.json();
    return data;
  };



  return (

    <div className="comment">
      {comments.length ?
        comments?.map((comment) => {
          let dateParse = Date.parse(comment.updated_at)
          let data = new Date(dateParse)

          return (
            <li key={comment.id}>
              <div className="upperData">
                <div className="gameTitle">
                  Game:
                  <p> {comment.game.name} </p>
                </div>
                <div className="gameTitle">
                  Date:
                  <p> {data.toDateString()}</p>
                </div>
              </div>
              <div>
                Body:  {comment.body}
              </div>
            </li>
          )

        }) : null


      }

      <Loader apiState={apiCommentsState}></Loader>

    </div>

  );
}

export default Comment