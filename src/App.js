import React, { useState, useEffect } from "react";
import DetailsPage from "./pages/DetailsPage";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import Main from "./pages/Main";
import Loader from "./components/Loader/Loader";
import Login from "./pages/Login"
import "./index.scss";
import SingUp from "./pages/SingUp";

function App() {
  const [currentPage, setCurrentPage] = useState(1);
  const [currentRoute, setCurrentRoute] = useState("Home");
  const [apiState, setApiState] = useState("loading");
  const [loginStatus, setLoginStatus] = useState("SessionOFF")
  const [userData, setUserData] = useState("NotUser");


  return (
    <div className="app">
      <Header setCurrentRoute={setCurrentRoute}
        loginStatus={loginStatus}
        userData={userData}
      ></Header>
      <Main
        currentPage={currentPage}
        setCurrentRoute={setCurrentRoute}
        pageSelection={currentRoute === "Home" ? "visible" : "invisible"}
        setApiState={setApiState}
        apiState={apiState}
      ></Main>
      <DetailsPage
        pageSelection={currentRoute === "Details" ? "visible" : "invisible"}
      ></DetailsPage>
      <Loader apiState={apiState}></Loader>
      <Footer
        currentPage={currentPage}
        setCurrentPage={setCurrentPage}
        pageSelection={currentRoute === "Home" ? "visible" : "invisible"}
      ></Footer>

      <Login
        setCurrentRoute={setCurrentRoute}
        pageSelection={currentRoute === "Login" ? "visible" : "invisible"}
        setLoginStatus={setLoginStatus}
        loginStatus={loginStatus}
        setUserData={setUserData}
      ></Login>

      <SingUp
        setCurrentRoute={setCurrentRoute}
        pageSelection={currentRoute === "SingUp" ? "visible" : "invisible"}>

      </SingUp>
    </div>
  );
}

export default App;
