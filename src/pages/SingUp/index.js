import React from "react";
import './styleSingUp.scss'

const SingUp = (props) => {
  const { setCurrentRoute, pageSelection } = props
  let userRegister = {
    firstName: '',
    lastName: '',
    email: '',
    password: '',
    confirmPassword: ''
  }

  const handleChange = (page) => {
    setCurrentRoute(page);
  }
  const handleSubmit = e => {
    e.preventDefault();
    console.log("works");
    console.log(userRegister);
  }

  return (
    <div className={`singUp-${pageSelection}`}>
      <div className="containerSingUp">
        <form className="form" onSubmit={handleSubmit}>
          <h1>SING UP</h1>

          <div className="form-group">
            <label>First Name</label>
            <input type="text" className="form-control" placeholder="First Name"
              onChange={e => userRegister.firstName = e.target.value}></input>
          </div>

          <div className="form-group">
            <label>Last Name</label>
            <input type="text" className="form-control" placeholder="Last Name"
              onChange={e => userRegister.lastName = e.target.value} ></input>
          </div>

          <div className="form-group">
            <label>Email</label>
            <input type="email" className="form-control" placeholder="Email"
              onChange={e => userRegister.email = e.target.value}></input>
          </div>

          <div className="form-group">
            <label>Password</label>
            <input type="password" className="form-control" placeholder="Password"
              onChange={e => userRegister.password = e.target.value}></input>
          </div>

          <div className="form-group">
            <label>Confirm password</label>
            <input type="password" className="form-control" placeholder="Confirm password"
              onChange={e => userRegister.confirmPassword = e.target.value}></input>
          </div>
          <div className="buttons">
            <button className="btn"> Sing Up</button>
            <a onClick={() => handleChange("Login")} className="link" > If you already have an account, click here </a>
          </div>
        </form>
      </div >
    </div >
  )
}

export default SingUp