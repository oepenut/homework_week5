import React from "react";
import "./Main.scss";
import DataFetchNext from "../Products";


const Main = (props) => {
  const { currentPage, setCurrentRoute, pageSelection, setApiState, apiState } = props;

  return (
    <div className={`main-${pageSelection}`}>
      {currentPage ? (
        <DataFetchNext
          currentPage={currentPage}
          setCurrentRoute={setCurrentRoute}
          setApiState={setApiState}
          apiState={apiState}
        />
      ) : ''}
    </div>
  );
};

export default Main;
