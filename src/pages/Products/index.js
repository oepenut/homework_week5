import React, { useState, useEffect } from "react";
import Card from "../../components/Cards/Card";


const DataFetchNext = (props) => {

  const { currentPage, setCurrentRoute, setApiState, apiState } = props;
  const [games, setGames] = useState([]);
  const [gamesPerPage, setGamesPerPage] = useState(5);


  useEffect(() => {

    (async () => {
      const dataBase = await ObtainData();
      setGames(dataBase);
      setApiState("Arrived")

    })();

  }, []);

  const indexOfLastGame = currentPage * gamesPerPage;
  const indexOfFirstGame = indexOfLastGame - gamesPerPage;
  const currentGames = games.slice(indexOfFirstGame, indexOfLastGame)

  const ObtainData = async () => {
    setApiState("loading");
    let URL = `https://trainee-gamerbox.herokuapp.com/games?_sort=id`
    const response = await fetch(`${URL}`);
    const data = await response.json();
    return data;
  };

  return (
    <div>
      <div>
        <h1 className="title-products">Products</h1>
        <div className="products">
          {currentGames.length
            ? currentGames?.map((game) => {

              let img = game.cover_art?.formats.small?.url;
              if (!img) {
                img = "https://www.publicdomainpictures.net/pictures/280000/nahled/not-found-image-15383864787lu.jpg";
              }
              let gameObj = {
                id: game.id,
                title: game.name,
                thumbnail: img,
                comments: game.comments,
                date: game.created_at,
                category: game.genre.name,
                body: game.genre.name,
                platform: game.platform

              }
              return (
                <Card
                  key={gameObj.id}
                  id={gameObj.id}
                  title={gameObj.title}
                  imgUrl={gameObj.thumbnail}
                  body={gameObj.body}
                  platform={gameObj.platform}
                  date={gameObj.date}
                  category={gameObj.category}
                  comments={gameObj.comments}
                  setCurrentRoute={setCurrentRoute}
                  apiState={apiState}
                ></Card>
              );
            })
            : null}
        </div>
      </div>
    </div>
  );

};

export default DataFetchNext;
