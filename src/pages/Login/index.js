import React, { useState, useEffect } from 'react'
import useFetch from 'react-fetch-hook'
import "./styleLogin.scss"


const Login = (props) => {

  const { setCurrentRoute, pageSelection, setLoginStatus, loginStatus, setUserData } = props

  const [apiStatus, setApistatus] = useState("loading")



  let userLogin = {
    identifier: '',
    password: ''
  }

  useEffect(() => {

  }, [])

  const handleChange = (page) => {
    setCurrentRoute(page);
  }

  const handleSubmit = e => {
    e.preventDefault();
    (async () => {
      setApistatus("loading");
      try {
        const response = await fetch("https://trainee-gamerbox.herokuapp.com/auth/local", {
          headers: {
            Accept: " */*",
            "Content-Type": "application/json",
          },
          method: "POST",
          body: JSON.stringify(userLogin),
        });
        if ((response.status === 200) | 201) {
          const data = await response.json();
          setUserData(data)
          setApistatus("Arrived")
          setLoginStatus("SessionON")
          localStorage.setItem('token', data.jwt)

        } else if (response.status === 400) {
          console.log("The user is incorrect!");
        }
      } catch (e) {
        console.log(e);
      }

    })();
  }


  return (
    <div className={`login-${pageSelection}`}>
      <div className='containerLogin'>
        <form className={`formLogin ${loginStatus === "SessionON" ? "ON" : ''}`} onSubmit={handleSubmit}>
          <h1>LOGIN</h1>

          <div className="form-group">
            <label>User:</label>
            <input type="text" className="form-control" placeholder="First Name"
              onChange={e => userLogin.identifier = e.target.value}></input>
          </div>


          <div className="form-group">
            <label>Password</label>
            <input type="password" className="form-control" placeholder="Password"
              onChange={e => userLogin.password = e.target.value}></input>
          </div>


          <div className="buttons">
            <button className="btn"> Login</button>
            <a onClick={() => handleChange("SingUp")} className="link"> If you are not registered, click here </a>
          </div>
        </form>
      </div>
    </div>
  )
}

export default Login
