This application is implemented with vercel in the following link:

https://homework-week5.vercel.app/

This app is a website about a video game store, where you can see all game files and select a game and read its specific features.
